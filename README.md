# Spack Tests

This package contains various scripts to use
the LCG spack installation on /cvmfs/sw.hsf.org

## Files

  * `mkview` - a script to (re)create an LCG view
  * `mkexternals` - a script to create an "LCG_external...txt" file
  * `upstreams.yaml` - definition of upstream spack db
  * `lcg.yaml` - projection file for LCG release
  * `contrib.yaml` - projection file for contrib

## Usage

    ./mkview [-v] [-p projection-file] [ <dir> [ <spec>... ]]

Creates a view similar the /cvmfs/sft.cern.ch/lcg/releases/LCG_101
area. Note that it will install `spack` if it can't find the 
`spack` command. It will also install the `sft-spack-repo`
package if it doesn't exist yet. 

This should be the preferred way to avoid possible interference with
another spack installation.

Run all examples out of this directory.

### Unified view

    ./mkview -v 

creates a unified view. This does not work at the moment as 
ROOT installs a `README/` directory while other packages install
a top level `README` file.

    spack python mkexternal [ <spec> ] >  releases/LCG_101/LCG_externals_x86_64-centos7-gcc8-opt.txt

Generates the text file as expected from the standard LCG releases.

### Defaults

  * The default spack package is `lcg-release@101-spack01`.
  * The default install directory is `releases/LCG_101`. 
  * The default projection file is `lcg.yaml`.

### Examples

    ./mkview

Generates a `releases/LCG_101` subdirectory.

You can use the script in fact for any spec you like:

    ./mkview root-installation root
    ./mkview -p contrib.yaml contrib gcc cmake ninja git

Note that you can install multiple versions of a package
if you have a projection file, but for a unified view
you typically have to specify the exact version of
the package you want:

    ./mkview -v tools cmake@3.20.0 gcc ninja

## ATLAS tdaq-common project

To build the tdaq-common project with the local `contrib` and
`release` area:

    ./mkview
    ./mkview -p contrib.yaml contrib gcc cmake ninja git
    ./mkexternal > releases/LCG_101/LCG_externals_x86_64-centos7-gcc8-opt.txt

    export LCG_RELEASE_BASE=$(pwd)/releases
    source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/setup.sh x86_64-centos7-gcc8-opt
    export CC=$(which gcc)
    export CXX=$(which g++)
    instdir=$(readlink -f .)/inst/tdaq-common/tdaq-common-99-00-00
    mkdir build
    cd build
    cmake \
       -D BINARY_TAG=x86_64-centos7-gcc8-opt \
       -D LCG_RELEASE_BASE=${LCG_RELEASE_BASE} \
       -D CMAKE_INSTALL_PREFIX=${instdir} \
       /cvmfs/atlas-online-nightlies.cern.ch/tdaq/nightlies/tdaq-common/tdaq-common-99-00-00
    make -j $(nproc) install

To use it:

    export LCG_INST_PATH=${LCG_RELEASE_BASE}
    export TDAQ_RELEASE_BASE=$(dirname $(dirname $instdir))
    export CMTCONFIG=x86_64-centos7-gcc8-opt
    source ${instdir}/setup.sh
 
or

    source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-common-99-00-00 x86_64-centos7-gcc8-opt

## ATLAS tdaq project

Start in the tdaq-spack area:

    export CMAKE_PREFIX_PATH=$(dirname $(dirname ${instdir})):/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/cmake
    export instdir2=$(readlink -f .)/inst/tdaq/tdaq-99-00-00
    mkdir build2
    cd build2
    cmake \
       -D BINARY_TAG=x86_64-centos7-gcc8-opt \
       -D LCG_RELEASE_BASE=${LCG_RELEASE_BASE} \
       -D CMAKE_INSTALL_PREFIX=${instdir2} \
       /cvmfs/atlas-online-nightlies.cern.ch/tdaq/nightlies/tdaq/tdaq-99-00-00
    make -k -j $(nproc) install

See [ERRORS.md](ERRORS.md) for problems.
