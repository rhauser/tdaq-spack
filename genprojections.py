#!/usr/bin/env python3
import sys
import os
import csv

def parse_translator():
  with open('lcgmake_spack_translator.csv', newline='') as csvfile:
    packagename_dict = {}
    rdr = csv.reader(filter(lambda row: row[0]!='#', csvfile))
    for row in rdr:
          if row[0].strip() and row[1].strip():
            packagename_dict[row[0].strip()] = row[1].strip()
  return packagename_dict

def write_projection(packagenames):
  print('projections:')
  print("  all: '{name}/{version}/{target}-{os}-gcc8-opt'")
  for old, new in packagenames.items():
    if old != new and not new.startswith('py-') and not new.startswith('r-'):
      print("  %s: %s/{version}/{target}-{os}-gcc8-opt" % (new, old))

if __name__ == "__main__":
  packagename_dict = parse_translator()
  write_projection(packagename_dict)

