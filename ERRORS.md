# Errors while building TDAQ

## Summary of Version Issues

### Java 1.8 is needed vs. Java 1.11 in spack release

Hacked locally by adding Java 1.8 into the view and
modifying LCG_externals file by hand.

### Qt5 is newer version that LCG 101

### Python version is 3.7.8  (3.9.6 in LCG)

### Boost is missing libraries vs. LCG 101

container, context, fiber, all math libs

## tdaq-common Build

### tdaq-commonConfig.cmake (when used from tdaq project) - [FIXED]

Missing boost libraries vs. LCG, should use find_package(... OPTIONAL_COMPONENTS ) to keep working in such situations 

## tdaq Build

### Several packages

Warnings from ld about possible incompatibe libcrypto.10 and libcrypto.1.1

This is most likely from mixing up system libraries that link against openssl (1.0.2)
with lcg-release libraries linked against openssl 1.1.1

### trpgui and oph

    /cvmfs/sw.hsf.org/contrib/binutils/2.37/x86_64-centos7-gcc4.8.5-opt/e44ldsiooccrs2h2eylg3br24u74dj23/bin/ld: /cvmfs/sw.hsf.org/spackages4/harfbuzz/1.9.0/x86_64-centos7-gcc8.3.0-opt/nofgjzz/lib/libharfbuzz.so.0: undefined reference to `FT_Done_MM_Var'

### ipbus (external package)

lots of boost errors, must be confused setup ?

### dqm_display [FIXED]

I suspect this is due to the different Qt5 version:

    /home/rhauser/tdaq/tdaq/tdaq-99-00-00/dqm_display/src/display/IconsViewDelegate.cpp: In member function 'void dqm_display::gui::IconsViewDelegate::doPaint(QPainter*, const QStyleOptionViewItem&, const QModelIndex&, int) const':
    /home/rhauser/tdaq/tdaq/tdaq-99-00-00/dqm_display/src/display/IconsViewDelegate.cpp:103:22: error: aggregate 'QPainterPath img' has incomplete type and cannot be defined
    /home/rhauser/tdaq/tdaq/tdaq-99-00-00/dqm_display/src/display/IconsViewDelegate.cpp:107:22: error: aggregate 'QPainterPath shadow' has incomplete type and cannot be defined
    /home/rhauser/tdaq/tdaq/tdaq-99-00-00/dqm_display/src/display/IconsViewDelegate.cpp:113:22: error: aggregate 'QPainterPath mask' has incomplete type and cannot be defined


