#!/bin/bash
#
usage()
{
  cat <<EOF
$0 [ -v ] [-p projections] [ <dir> [ <spec>... ]]

Create a view for an LCG release.

   -v Create a unified view <dir>/[bin,lib,...] else create an <dir>/<pkg>/<version>/<config> view.

   -p <file> Specify a spack projections file.

   <spec> should be of the form lcg-release@version for LCG
      but in fact can be any available number of specs

   <dir> is the local top level directory (releases/LCG_101 by default)

EOF
}

[ -n "$SPACK_ENV" ] && { echo "Do not run this in an spack environment" >&2; exit 1; }

dir=$(dirname $(readlink -f $0))
projections="--projection-file ${dir}/lcg.yaml"

case "$1" in
  -v)
    projections=""
    shift
    ;;
  -h|--help)
    usage
    exit
    ;;
  -p)
    [ -n "$2" -a -f "$2" ] || { echo "Project file does not exist: $2" >&2 ; exit 1; }
    projections="--projection-file ${2}"
    shift 2
    ;;
  -*)
    usage
    exit 1
    ;;
esac

lcg=${1:-releases/LCG_101}
shift
release=$@
[ -z "${release}" ] && release=lcg-release@101-spack01

[ -d ${lcg} ] && { echo "Directory ${lcg} exists, this will lead to conflicts" >&2; exit 1; }

echo "Installing ${release} into ${lcg}"

if ! command -v spack > /dev/null
then
  if [ ! -d spack ]
  then
    echo "Getting spack"
    git clone --depth 1 https://github.com/spack/spack.git || exit 1
  fi
  echo "Setting up spack"
  source spack/share/spack/setup-env.sh
fi
spack --version

if ! spack repo list | grep -q ^lcg
then
  if [ ! -d sft-spack-repo ]
  then
    echo "Getting SFT spack repo"
    git clone https://:@gitlab.cern.ch:8443/sft/sft-spack-repo.git || exit 2
  fi
  echo "Adding SFT spack repo"
  spack repo add --scope=site sft-spack-repo || exit 2
fi

# This is the important part:
# Find the requested spec in the upstream packages, get the exact hash and
# then create a view to your liking with it.
spec=$(spack -C ${dir} find --format "{name}@{version}/{hash}" ${release}) || exit 3
echo "Found requested version: ${spec}"

echo "Creating view"
spack -C ${dir} view add -i ${projections} ${lcg} ${spec}
